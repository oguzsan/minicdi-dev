using System;

namespace Minic.DI.Test.Payloads
{
    public class SimpleClassB : ISimpleInterfaceB
    {
        public int Value;
    }
}
